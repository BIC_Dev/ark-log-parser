package logparser

import (
	"bufio"
	"io"
	"time"

	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/admin"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/chat"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/pve"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/pvp"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/tribe"
)

type ArkLogs struct {
	Details ArkLogDetails
	Admin   []admin.AdminLog
	Chat    []chat.ChatLog
	PVE     []pve.PVELog
	PVP     []pvp.PVPLog
	Tribe   []tribe.TribeLog
	Alerts  interface{}
}

type ArkLogDetails struct {
	TotalLines           int32
	ParsedLines          int32
	ParseDurationSeconds float32
	LastTimestamp        int64
}

func ParseLogs(r io.Reader) (*ArkLogs, error) {
	startTime := time.Now().UnixMilli()

	// Output Struct
	logOutput := &ArkLogs{}

	// ADMIN
	adminAdmin := &admin.Admin{}

	// CHAT
	chatChat := &chat.Chat{}

	// PVE
	pveDinoKilledPlayer := &pve.DinoKilledPlayer{}
	pvePlayerDie := &pve.PlayerDie{}

	// PVP
	pvpDinoKilledPlayer := &pvp.DinoKilledPlayer{}
	pvpPlayerKilledPlayer := &pvp.PlayerKilledPlayer{}

	// TRIBE
	tribeDinoKilledDino := &tribe.DinoKilledDino{}
	tribePlayerOrDinoKilledDino := &tribe.PlayerOrDinoKilledDino{}
	tribePlayerKilledStructure := &tribe.PlayerKilledStructure{}
	tribeDinoDie := &tribe.DinoDie{}
	tribeDinoKilledStructure := &tribe.DinoKilledStructure{}
	tribeDinoKilledPlayer := &tribe.DinoKilledPlayer{}

	var lineCount int32 = 0
	var successfulParses int32 = 0
	var lastTimestamp int64

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		lineCount++

		// TRIBE
		if ok, _ := tribeDinoKilledDino.Extract(scanner.Bytes()); ok {
			logOutput.Tribe = append(logOutput.Tribe, tribe.TribeLog{
				LogType:       "tribe_dino_killed_dino",
				EventNum:      string(tribeDinoKilledDino.EventNum),
				Time:          tribeDinoKilledDino.Time,
				KilledTribe:   string(tribeDinoKilledDino.KilledTribe),
				KilledTribeID: string(tribeDinoKilledDino.KilledTribeID),
				InGameDay:     string(tribeDinoKilledDino.InGameDay),
				InGameTime:    string(tribeDinoKilledDino.InGameTime),
				KilledName:    string(tribeDinoKilledDino.KilledName),
				KilledLevel:   string(tribeDinoKilledDino.KilledLevel),
				KilledType:    string(tribeDinoKilledDino.KilledType),
				KillerName:    string(tribeDinoKilledDino.KillerName),
				KillerLevel:   string(tribeDinoKilledDino.KillerLevel),
				KillerType:    string(tribeDinoKilledDino.KillerType),
				KillerTribe:   string(tribeDinoKilledDino.KillerTribe),
			})

			lastTimestamp = tribeDinoKilledDino.Time
			successfulParses++
			continue
		}

		if ok, _ := tribePlayerOrDinoKilledDino.Extract(scanner.Bytes()); ok {
			logOutput.Tribe = append(logOutput.Tribe, tribe.TribeLog{
				LogType:       "tribe_player_or_dino_killed_dino",
				EventNum:      string(tribePlayerOrDinoKilledDino.EventNum),
				Time:          tribePlayerOrDinoKilledDino.Time,
				KilledTribe:   string(tribePlayerOrDinoKilledDino.KilledTribe),
				KilledTribeID: string(tribePlayerOrDinoKilledDino.KilledTribeID),
				InGameDay:     string(tribePlayerOrDinoKilledDino.InGameDay),
				InGameTime:    string(tribePlayerOrDinoKilledDino.InGameTime),
				KilledName:    string(tribePlayerOrDinoKilledDino.KilledName),
				KilledLevel:   string(tribePlayerOrDinoKilledDino.KilledLevel),
				KilledType:    string(tribePlayerOrDinoKilledDino.KilledType),
				KillerName:    string(tribePlayerOrDinoKilledDino.KillerName),
				KillerLevel:   string(tribePlayerOrDinoKilledDino.KillerLevel),
				KillerTribe:   string(tribePlayerOrDinoKilledDino.KillerTribeOrType),
			})

			lastTimestamp = tribePlayerOrDinoKilledDino.Time
			successfulParses++
			continue
		}

		if ok, _ := tribePlayerKilledStructure.Extract(scanner.Bytes()); ok {
			if !tribePlayerKilledStructure.KillerTribe.Valid {
				tribePlayerKilledStructure.KillerTribe.Tribe = []byte{}
			}

			logOutput.Tribe = append(logOutput.Tribe, tribe.TribeLog{
				LogType:         "tribe_player_killed_structure",
				EventNum:        string(tribePlayerKilledStructure.EventNum),
				Time:            tribePlayerKilledStructure.Time,
				KilledTribe:     string(tribePlayerKilledStructure.StructureTribe),
				KilledTribeID:   string(tribePlayerKilledStructure.StructureTribeID),
				InGameDay:       string(tribePlayerKilledStructure.InGameDay),
				InGameTime:      string(tribePlayerKilledStructure.InGameTime),
				KillerName:      string(tribePlayerKilledStructure.KillerName),
				KillerLevel:     string(tribePlayerKilledStructure.KillerLevel),
				KillerTribe:     string(tribePlayerKilledStructure.KillerTribe.Tribe),
				StructureKilled: string(tribePlayerKilledStructure.StructureKilled),
			})

			lastTimestamp = tribePlayerKilledStructure.Time
			successfulParses++
			continue
		}

		if ok, _ := tribeDinoKilledStructure.Extract(scanner.Bytes()); ok {
			logOutput.Tribe = append(logOutput.Tribe, tribe.TribeLog{
				LogType:       "tribe_dino_killed_structure",
				EventNum:      string(tribeDinoKilledStructure.EventNum),
				Time:          tribeDinoKilledStructure.Time,
				KilledTribe:   string(tribeDinoKilledStructure.KilledTribe),
				KilledTribeID: string(tribeDinoKilledStructure.KilledTribeID),
				InGameDay:     string(tribeDinoKilledStructure.InGameDay),
				InGameTime:    string(tribeDinoKilledStructure.InGameTime),
				KilledName:    string(tribeDinoKilledStructure.KilledName),
				KilledType:    string(tribeDinoKilledStructure.KilledType),
				KillerName:    string(tribeDinoKilledStructure.KillerName),
				KillerLevel:   string(tribeDinoKilledStructure.KillerLevel),
			})

			lastTimestamp = tribeDinoKilledStructure.Time
			successfulParses++
			continue
		}

		if ok, _ := tribeDinoKilledPlayer.Extract(scanner.Bytes()); ok {
			logOutput.Tribe = append(logOutput.Tribe, tribe.TribeLog{
				LogType:       "tribe_dino_killed_player",
				EventNum:      string(tribeDinoKilledPlayer.EventNum),
				Time:          tribeDinoKilledPlayer.Time,
				KilledTribe:   string(tribeDinoKilledPlayer.KilledTribe),
				KilledTribeID: string(tribeDinoKilledPlayer.KilledTribeID),
				InGameDay:     string(tribeDinoKilledPlayer.InGameDay),
				InGameTime:    string(tribeDinoKilledPlayer.InGameTime),
				KilledName:    string(tribeDinoKilledPlayer.KilledName),
				KilledLevel:   string(tribeDinoKilledPlayer.KilledLevel),
				KillerName:    string(tribeDinoKilledPlayer.KillerName),
				KillerLevel:   string(tribeDinoKilledPlayer.KillerLevel),
				KillerType:    string(tribeDinoKilledPlayer.KillerType),
				KillerTribe:   string(tribeDinoKilledPlayer.KillerTribe),
			})

			lastTimestamp = tribeDinoKilledPlayer.Time
			successfulParses++
			continue
		}

		if ok, _ := tribeDinoDie.Extract(scanner.Bytes()); ok {
			logOutput.Tribe = append(logOutput.Tribe, tribe.TribeLog{
				LogType:       "tribe_dino_die",
				EventNum:      string(tribeDinoDie.EventNum),
				Time:          tribeDinoDie.Time,
				KilledTribe:   string(tribeDinoDie.KilledTribe),
				KilledTribeID: string(tribeDinoDie.KilledTribeID),
				InGameDay:     string(tribeDinoDie.InGameDay),
				InGameTime:    string(tribeDinoDie.InGameTime),
				KilledName:    string(tribeDinoDie.KilledName),
				KilledLevel:   string(tribeDinoDie.KilledLevel),
				KilledType:    string(tribeDinoDie.KilledType),
			})

			lastTimestamp = tribeDinoDie.Time
			successfulParses++
			continue
		}

		// ADMIN
		if ok, _ := adminAdmin.Extract(scanner.Bytes()); ok {
			logOutput.Admin = append(logOutput.Admin, admin.AdminLog{
				LogType:    "admin",
				EventNum:   string(adminAdmin.EventNum),
				Time:       adminAdmin.Time,
				Command:    string(adminAdmin.Command),
				PlayerName: string(adminAdmin.PlayerName),
				ARKID:      string(adminAdmin.ARKID),
				SteamID:    string(adminAdmin.SteamID),
			})

			lastTimestamp = adminAdmin.Time
			successfulParses++
			continue
		}

		// CHAT
		if ok, _ := chatChat.Extract(scanner.Bytes()); ok {
			logOutput.Chat = append(logOutput.Chat, chat.ChatLog{
				LogType:     "chat",
				EventNum:    string(chatChat.EventNum),
				Time:        chatChat.Time,
				AccountName: string(chatChat.AccountName),
				UserName:    string(chatChat.UserName),
				Message:     string(chatChat.Message),
			})

			lastTimestamp = chatChat.Time
			successfulParses++
			continue
		}

		// PVP
		if ok, _ := pvpDinoKilledPlayer.Extract(scanner.Bytes()); ok {
			logOutput.PVP = append(logOutput.PVP, pvp.PVPLog{
				LogType:           "pvp_dino_killed_player",
				EventNum:          string(pvpDinoKilledPlayer.EventNum),
				Time:              pvpDinoKilledPlayer.Time,
				KilledPlayerName:  string(pvpDinoKilledPlayer.KilledPlayerName),
				KilledPlayerLevel: string(pvpDinoKilledPlayer.KilledPlayerLevel),
				KilledPlayerTribe: string(pvpDinoKilledPlayer.KilledPlayerTribe),
				KillerName:        string(pvpDinoKilledPlayer.KillerName),
				KillerLevel:       string(pvpDinoKilledPlayer.KillerLevel),
				KillerDinoType:    string(pvpDinoKilledPlayer.KillerDinoType),
				KillerTribe:       string(pvpDinoKilledPlayer.KillerTribe),
				KillerSID:         string(pvpDinoKilledPlayer.KillerSID),
			})

			lastTimestamp = pvpDinoKilledPlayer.Time
			successfulParses++
			continue
		}

		if ok, _ := pvpPlayerKilledPlayer.Extract(scanner.Bytes()); ok {
			logOutput.PVP = append(logOutput.PVP, pvp.PVPLog{
				LogType:           "pvp_player_killed_player",
				EventNum:          string(pvpDinoKilledPlayer.EventNum),
				Time:              pvpDinoKilledPlayer.Time,
				KilledPlayerName:  string(pvpDinoKilledPlayer.KilledPlayerName),
				KilledPlayerLevel: string(pvpDinoKilledPlayer.KilledPlayerLevel),
				KilledPlayerTribe: string(pvpDinoKilledPlayer.KilledPlayerTribe),
				KillerName:        string(pvpDinoKilledPlayer.KillerName),
				KillerLevel:       string(pvpDinoKilledPlayer.KillerLevel),
				KillerTribe:       string(pvpDinoKilledPlayer.KillerTribe),
				KillerSID:         string(pvpDinoKilledPlayer.KillerSID),
			})

			lastTimestamp = pvpPlayerKilledPlayer.Time
			successfulParses++
			continue
		}

		// PVE
		if ok, _ := pveDinoKilledPlayer.Extract(scanner.Bytes()); ok {
			logOutput.PVE = append(logOutput.PVE, pve.PVELog{
				LogType:           "pve_dino_killed_player",
				EventNum:          string(pveDinoKilledPlayer.EventNum),
				Time:              pveDinoKilledPlayer.Time,
				KilledPlayerName:  string(pveDinoKilledPlayer.KilledPlayerName),
				KilledPlayerLevel: string(pveDinoKilledPlayer.KilledPlayerLevel),
				KilledPlayerTribe: string(pveDinoKilledPlayer.KilledPlayerTribe),
				KillerName:        string(pveDinoKilledPlayer.KillerName),
				KillerLevel:       string(pveDinoKilledPlayer.KillerLevel),
			})

			lastTimestamp = pveDinoKilledPlayer.Time
			successfulParses++
			continue
		}

		if ok, _ := pvePlayerDie.Extract(scanner.Bytes()); ok {
			if !pvePlayerDie.KillerSID.Valid {
				pvePlayerDie.KillerSID.SID = []byte{}
			}

			logOutput.PVE = append(logOutput.PVE, pve.PVELog{
				LogType:           "pve_player_die",
				EventNum:          string(pvePlayerDie.EventNum),
				Time:              pvePlayerDie.Time,
				KilledPlayerName:  string(pvePlayerDie.KilledPlayerName),
				KilledPlayerLevel: string(pvePlayerDie.KilledPlayerLevel),
				KilledPlayerTribe: string(pvePlayerDie.KilledPlayerTribe),
				KillerSID:         string(pvePlayerDie.KillerSID.SID),
			})

			lastTimestamp = pvePlayerDie.Time
			successfulParses++
			continue
		}
	}

	endTime := time.Now().UnixMilli()

	logOutput.Details = ArkLogDetails{
		TotalLines:           lineCount,
		ParsedLines:          successfulParses,
		ParseDurationSeconds: float32(endTime-startTime) / 1000,
		LastTimestamp:        lastTimestamp,
	}

	return logOutput, nil
}
