package logparser

import (
	"os"
	"testing"
)

func TestParseLogs(t *testing.T) {
	file, foErr := os.Open("../examples/full-large.log")
	if foErr != nil {
		t.Errorf("Failed to open test file: %s", foErr)
	}

	defer file.Close()

	logs, err := ParseLogs(file)
	if err != nil {
		t.Errorf("Failed to parse logs: %s", err)
	}

	t.Logf("Total Lines: %d", logs.Details.TotalLines)
	t.Logf("Lines Parsed: %d", logs.Details.ParsedLines)
	t.Logf("Duration: %.4f", logs.Details.ParseDurationSeconds)
	t.Logf("Latests Timestamp: %d", logs.Details.LastTimestamp)
	t.Logf("Total Admin Logs: %d", len(logs.Admin))
	t.Logf("Total Chat Logs: %d", len(logs.Chat))
	t.Logf("Total PVP Logs: %d", len(logs.PVP))
	t.Logf("Total PVE Logs: %d", len(logs.PVE))
	t.Logf("Total Tribe Logs: %d", len(logs.Tribe))
}
