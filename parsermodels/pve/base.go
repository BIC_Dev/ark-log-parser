package pve

import (
	"time"
)

type PVELog struct {
	LogType           string
	EventNum          string
	Time              int64
	KilledPlayerName  string
	KilledPlayerLevel string
	KilledPlayerTribe string
	KillerName        string
	KillerLevel       string
	KillerSID         string
}

func (p *DinoKilledPlayer) unmarshalTime(value []byte) (int64, error) {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, string(value))

	if err != nil {
		return 0, err
	}

	return t.Unix(), nil
}

func (p *PlayerDie) unmarshalTime(value []byte) (int64, error) {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, string(value))

	if err != nil {
		return 0, err
	}

	return t.Unix(), nil
}
