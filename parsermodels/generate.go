package parsermodels

//go:generate ldetool --package chat ./chat/Chat.lde
//go:generate ldetool --package admin ./admin/Admin.lde
//go:generate ldetool --package pvp ./pvp/PlayerKilledPlayer.lde
//go:generate ldetool --package pvp ./pvp/DinoKilledPlayer.lde
//go:generate ldetool --package tribe ./tribe/PlayerKilledStructure.lde
//go:generate ldetool --package pve ./pve/DinoKilledPlayer.lde
//go:generate ldetool --package tribe ./tribe/DinoKilledDino.lde
//go:generate ldetool --package pve ./pve/PlayerDie.lde
//go:generate ldetool --package tribe ./tribe/DinoDie.lde
//go:generate ldetool --package tribe ./tribe/DinoKilledStructure.lde
//go:generate ldetool --package tribe ./tribe/DinoKilledPlayer.lde
//go:generate ldetool --package tribe ./tribe/PlayerOrDinoKilledDino.lde
