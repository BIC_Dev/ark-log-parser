package tribe

import (
	"time"
)

type TribeLog struct {
	LogType         string
	EventNum        string
	Time            int64
	KilledTribe     string
	KilledTribeID   string
	InGameDay       string
	InGameTime      string
	KilledName      string
	KilledLevel     string
	KilledType      string
	KillerName      string
	KillerLevel     string
	KillerType      string
	KillerTribe     string
	StructureKilled string
}

func (p *DinoDie) unmarshalTime(value []byte) (int64, error) {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, string(value))

	if err != nil {
		return 0, err
	}

	return t.Unix(), nil
}

func (p *DinoKilledDino) unmarshalTime(value []byte) (int64, error) {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, string(value))

	if err != nil {
		return 0, err
	}

	return t.Unix(), nil
}

func (p *DinoKilledPlayer) unmarshalTime(value []byte) (int64, error) {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, string(value))

	if err != nil {
		return 0, err
	}

	return t.Unix(), nil
}

func (p *DinoKilledStructure) unmarshalTime(value []byte) (int64, error) {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, string(value))

	if err != nil {
		return 0, err
	}

	return t.Unix(), nil
}

func (p *PlayerOrDinoKilledDino) unmarshalTime(value []byte) (int64, error) {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, string(value))

	if err != nil {
		return 0, err
	}

	return t.Unix(), nil
}

func (p *PlayerKilledStructure) unmarshalTime(value []byte) (int64, error) {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, string(value))

	if err != nil {
		return 0, err
	}

	return t.Unix(), nil
}
