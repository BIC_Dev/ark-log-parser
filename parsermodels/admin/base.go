package admin

import (
	"time"
)

type AdminLog struct {
	LogType    string `json:"log_type"`
	EventNum   string `json:"event_num"`
	Time       int64  `json:"time"`
	Command    string `json:"command"`
	PlayerName string `json:"player_name"`
	ARKID      string `json:"ark_id"`
	SteamID    string `json:"steam_id"`
}

func (p *Admin) unmarshalTime(value []byte) (int64, error) {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, string(value))

	if err != nil {
		return 0, err
	}

	return t.Unix(), nil
}
