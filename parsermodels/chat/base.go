package chat

import (
	"time"
)

type ChatLog struct {
	LogType     string `json:"log_type"`
	EventNum    string `json:"event_num"`
	Time        int64  `json:"time"`
	AccountName string `json:"account_name"`
	UserName    string `json:"user_name"`
	Message     string `json:"message"`
}

func (p *Chat) unmarshalTime(value []byte) (int64, error) {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, string(value))

	if err != nil {
		return 0, err
	}

	return t.Unix(), nil
}
