# Log Parsing Testbed

# Log Categories

## Admin Logs
1. Admin log events (all of them) `/admin/Admin.lde` (DONE)

## Chat Logs
1. Chat log events (all of them) `/chat/Chat.lde` (DONE)

## PvP Logs
1. Player kill Player `/pvp/PlayerKilledPlayer.lde` (DONE)
2. Tribe Dino kill Player `/pvp/TribeDinoKilledPlayer.lde` (DONE)

## Tribe Logs
1. Player kill Tribe Dino `/tribe/PlayerKilledDino.lde` (DONE)
2. Player kill Tribe Structure `/tribe/PlayerKilledStructure.lde` (DONE)
3. Dino kill Tribe Dino `/tribe/DinoKilledDino.lde` (DONE)
4. Dino kill Player `/tribe/DinoKilledPlayer.lde` (DONE)
5. Dino claiming `/tribe/DinoClaiming.lde` (TBD)
6. Dino die `/tribe/DinoDie.lde` (DONE)
7. Dino kill Structure `/tribe/DinoKilledStructure.lde` (DONE)

## PvE Logs
1. Dino (no tribe) kill Player `/pve/DinoKilledPlayer.lde` (DONE)
2. Player die (with no reason) `/pve/PlayerDie.lde` (DONE)

## Alerts
1. Player found with duplicate item `/alert/DuplicateItem.lde` (NEED EXAMPLE)


# Information
Decoding multiple structs from response: https://stackoverflow.com/questions/47911187/golang-elegantly-json-decode-different-structures